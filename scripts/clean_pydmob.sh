#!/usr/bin/env bash
cd pydmob
rm -f *.o *.so *.a *.mod *.pyc *.pyo *.orig
rm -f */*.o */*.so */*.a */*.mod */*.pyc */*.pyo */*.orig
rm -f */*/*.o */*/*.so */*/*.a */*/*.mod */*/*.pyc */*/*.pyo */*/*.orig
