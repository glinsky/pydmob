import math
import os
import sys
import unittest

import nose.tools
import numpy
import numpy.testing
import pydmob.pygslib.kb2d



class kb2dTest(unittest.TestCase):

	def test_kb2d(self):
		ppoints = numpy.loadtxt(os.path.dirname(__file__)+'/cluster.dat')

		ixl=1
		iyl=2
		ivrl=3
		
		tmin=-1.0e21
		tmax=1.0e21 
		
		nx=5
		xmn=5.0
		xsiz=10.0
		
		ny=5
		ymn=5.0
		ysiz=10.0
		
		nxdis=1
		nydis=1
		
		ndmin=4
		ndmax=8
		
		radius=20
		
		ktype=1
		skmean=2.302
		
		nst=1
		c0=2.0
		
		it=numpy.array([1])
		cc=numpy.array([8.0])
		ang=numpy.array([0.0])
		aa=numpy.array([10.0])
		a2=numpy.array([10.0])
		
		pydmob.pygslib.kb2d.setparm(ppoints,ixl,iyl,ivrl,tmin,tmax,nx,xmn,xsiz,ny,ymn,ysiz,nxdis,nydis,ndmin,ndmax,radius,ktype,skmean,c0,it,cc,ang,aa,a2)
		pydmob.pygslib.kb2d.kb2d()
		
		# everything is rounded to two digits for the comparison
		
		kb2d_py=numpy.zeros([25,2])
		kb2d_py[:,0]=numpy.reshape(numpy.transpose(pydmob.pygslib.kb2d.datagv.vals[0:nx,0:ny,0]),(25))*1000.
		kb2d_py[:,1]=numpy.reshape(numpy.transpose(pydmob.pygslib.kb2d.datagv.vals[0:nx,0:ny,1]),(25))*1000.
		
		kb2d_py=kb2d_py.astype(numpy.integer)
		kb2d_py=numpy.around(kb2d_py.astype(numpy.float)/1000.,decimals=2)
				
		kb2d_out=numpy.array(
		  [[4.036,    6.784],
		   [0.233,    5.207],
		   [1.355,    4.558],
		   [0.653,    7.228],
		   [1.258,    6.000],
		   [3.142,    5.227],
		   [7.674,    4.841],
		   [0.564,    6.270],
		   [0.771,    4.688],
		   [0.861,    4.670],
		   [1.047,    4.824],
		   [0.898,    5.200],
		   [3.364,    3.942],
		   [0.873,    7.536],
		   [1.078,    5.963],
		   [1.044,    5.740],
		   [1.397,    5.813],
		   [2.178,    6.420],
		   [5.539,    4.554],
		   [2.876,    5.878],
		   [1.409,    6.298],
		   [1.081,    4.754],
		   [2.918,    7.408],
		   [4.880,    4.122],
		   [2.848,    6.451]
		   ])

		kb2d_out=kb2d_out*1000.
		kb2d_out=kb2d_out.astype(numpy.integer)
		kb2d_out=numpy.around(kb2d_out.astype(numpy.float)/1000.,decimals=2)

		kb2d_out-kb2d_py

		numpy.testing.assert_equal(kb2d_py,kb2d_out)