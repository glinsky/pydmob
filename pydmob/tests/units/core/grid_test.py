""" Tests for pyda.core.grid """

import os
import unittest

import pydmob.core.grid


class RegularGridCartesian2DTestCase(unittest.TestCase):


    def setUp(self):
        self.grid=pydmob.core.grid.RegularGridCartesian2D(x0=0,y0=0,nx=11,ny=11,dx=0.1,dy=0.1)
        
    def test_set_grid_to_value(self):
        self.grid.set_grid_to_value(5.0)
        self.assertEqual(self.grid.get_value(0,0),5.0)

    def test_get_linear_interpolated_value(self):
        self.grid.set_grid_to_value(5.0)
        self.assertEqual(self.grid.get_linear_interpolated_value(0.5,0.5),5.0)
        
if __name__ == '__main__':
    unittest.main()
