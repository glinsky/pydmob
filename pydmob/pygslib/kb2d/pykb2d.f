
      subroutine setparm(ppoints,ppoints_n1,ppoints_n2,
     .     ixl,iyl,ivrl,tmin,tmax,
     .     nx_,xmn_,xsiz_,ny_,ymn_,ysiz_,
     .     nxdis_,nydis_,ndmin_,ndmax_,
     .     radius_,ktype_,skmean_,nst_,
     .     c0_,it_,cc_,ang_,aa_,a2_)
     
c-----------------------------------------------------------------------
c     
c     Initialization for kb2d
c     ***********************
c     
c     The input parameters and data are set. Some quick error checking
c     is performed and the statistics of all the variables being considered 
c     are written to standard output for idbg.ge.1
c     
c-----------------------------------------------------------------------

      include  'kb2d.inc'
      
c     ppoints       - array with primary data
c     ixl,iyl,izl,ivrl - columns for x,y,z,vr in array
c     tmin,tmax     - trimming limits
c     nx_,xmn_,xsiz_ - output grid in x and y given as lower left 
c     ny_,ymn_,ysiz_   corner, number of points  and step size
c     nxdis_,nydis_ - x and y block discretization
c     ndmin_,ndmax_ - min and max data for kriging
c     radius_       - maximum search radius
c     ktype_        - (0=stationary simple kriging) (1=ordinary kriging)
c     skmean_       - mean for stationary simple kriging 
c     nst_          - number of nested structures (max 4)
c     c0_           - nugget effect (sill)
c     it_(nst)	    -  Type of each nested structure:
c                      1. spherical model of range a;
c                      2. exponential model of practical range a
c                      3. gaussian model of practical range a
c                      4. power model of power a (a must be gt. 0  and
c                           lt. 2).  if linear model, a=1,c=slope.
c     cc_(nst_)     -  Multiplicative factor of each nested structure.
c     ang_(nst_)    -  Azimuth angle for the principal direction of
c                      continuity (measured clockwise in degrees from Y)
c     aa_(nst_)     - principal axis of ellipse
c     a2_(nst_)     - minor axis of ellipse

      integer,intent(in) :: ppoints_n1,ppoints_n2
      real,intent(in) :: ppoints(ppoints_n1,ppoints_n2)
      real,intent(in) :: tmin,tmax
      integer,intent(in) :: nx_,ny_
      real,intent(in) :: xmn_,ymn_
      real,intent(in) :: xsiz_,ysiz_
      real,intent(in) :: nxdis_,nydis_
      integer,intent(in) :: ndmin_,ndmax_
      real,intent(in) :: radius_
      integer,intent(in) :: ktype_
      real,intent(in) :: skmean_
      integer,intent(in) :: nst_
      real,intent(in) :: c0_
      integer,intent(in) :: it_(nst_)
      real,intent(in) :: cc_(nst_),ang_(nst_),aa_(nst_),a2_(nst_)

cf2py real,intent(in) :: ppoints(ppoints_n1,ppoints_n2)
cf2py integer,intent(in) :: ixl,iyl,izl,ivrl
cf2py real,intent(in) :: tmin,tmax
cf2py integer,intent(in) :: nx_,ny_
cf2py real,intent(in) :: xmn_,ymn_,xsiz_,ysiz_
cf2py real,intent(in) :: nxdis_,nydis_
cf2py integer,intent(in) :: ndmin_,ndmax_
cf2py real,intent(in) :: radius_
cf2py integer,intent(in) :: ktype_
cf2py real,intent(in) :: skmean_
cf2py integer,intent(in) :: nst_
cf2py real,intent(in) :: c0_
cf2py integer,intent(in) :: it_(nst_)
cf2py real,intent(in) ::cc_(nst),ang_(nst_),aa_(nst_),a2_(nst_)

      parameter(MV=20)
      real      var(MV)

c     
c     Debugging flag
c     
      idbg=0

      if (idbg.ge.1) write(*,*) 
      if (idbg.ge.1) write(*,*) ' columns for X,Y, VR = ',ixl,iyl,ivrl
      if (idbg.ge.1) write(*,*) ' trimming limits = ',tmin,tmax
      if (idbg.ge.1) write(*,*) ' debugging level = ',idbg

      nx=nx_
      xmn=xmn_
      xsiz=xsiz_
      if (nx.gt.MAXX) stop 'ny is too big - modify .inc file'
      if (idbg.ge.1) write(*,*) ' nx, xmn, xsiz = ',nx,xmn,xsiz

      ny=ny_
      ymn=ymn_
      ysiz=ysiz_
      if (ny.gt.MAXY) stop 'ny is too big - modify .inc file'
      if (idbg.ge.1) write(*,*) ' ny, ymn, ysiz = ',ny,ymn,ysiz

      nxdis=nxdis_
      nydis=nydis_
      if (idbg.ge.1) write(*,*) ' discretization = ',nxdis,nydis

      ndmin=ndmin_
      ndmax=ndmax_
      if(ndmin.lt.0) ndmin = 0
      if (idbg.ge.1) write(*,*) ' min max data = ',ndmin,ndmax

      radius=radius_
      if (idbg.ge.1) write(*,*) ' isotropic radius = ',radius

      ktype=ktype_
      skmean=skmean_
      if (idbg.ge.1) write(*,*) ' ktype,skmea = ',ktype,skmean

      nst=nst_
      c0=c0_
      if (idbg.ge.1) write(*,*) ' nst, nugget = ',nst,c0

      if(nst.le.0) then
         nst     = 1
         it(1)   = 1
         cc(1)   = 0.0
         ang(1)  = 0.0
         aa(1)   = 0.0
         anis(1) = 0.0
      else
         do i=1,nst
            it(i)=it_(i)
            cc(i)=cc_(i)
            ang(i)=ang_(i)
            aa(i)=aa_(i)
            a2=a2_(i)
            anis(i) = a2 / aa(i)
            if (idbg.ge.1) write(*,*) ' it,cc,ang,a_max,a_min = ',
     +           it(i),cc(i),ang(i),aa(i),a2
            if(it(i).eq.4) then
               if(aa(i).lt.0.0) stop ' INVALID power variogram'
               if(aa(i).gt.2.0) stop ' INVALID power variogram'
            end if
         end do
      end if

      close(lin)
      write(*,*)
      if(nst.gt.MAXNST)   stop 'nst is too big - modify .inc file'
      if(ndmax.gt.MAXSAM) stop 'ndmax is too big - modify .inc file'

      nrows =ppoints_n1
      nvari=ppoints_n2
      
      av = 0.0
      ss = 0.0

c     Load the data:  
  
      nd = 0
      irow=0
 7    irow = irow +1
      if (irow.gt.nrows) goto  8
      do 1101 j=1,nvari
         var(j)=ppoints(irow,j)
 1101 continue 	  

      vrt = var(ivrl)

      if(vrt.lt.tmin.or.vrt.ge.tmax) go to 7
      nd = nd + 1
      if(nd.gt.MAXDAT) then
         write(*,*) ' ERROR: Exceeded available memory for data'
         stop
      end if
      x(nd)  = var(ixl)
      y(nd)  = var(iyl)
      vr(nd) = vrt
      av     = av + vrt
      ss     = ss + vrt*vrt
      go to 7
  
 8    av = av / max(real(nd),1.0)
      ss =(ss / max(real(nd),1.0)) - av * av
   
      if (idbg.ge.1) write(*,900) nd,av,sqrt(max(ss,0.0))
 900  format(/' There are ',i8,' data with:',/,
     +     '   mean value          = ',f12.5,/,
     +     '   standard deviation  = ',f12.5,/)
      return

      end