
import numpy
import core.grid

class Property:

    def __init__(self,x0,y0,nx,ny,dx,dy):

        self.x0=x0
        self.y0=y0
        self.nx=nx
        self.ny=ny
        self.dx=dx
        self.dy=dy
        
        self.gridded_val=core.grid.RegularGridCartesian2D(x0=self.x0,y0=self.y0,nx=self.nx,ny=self.ny,dx=self.dx,dy=self.dy)
        self.gridded_unc=core.grid.RegularGridCartesian2D(x0=self.x0,y0=self.y0,nx=self.nx,ny=self.ny,dx=self.dx,dy=self.dy)
        

class Horizon(Property):
    
    pass
