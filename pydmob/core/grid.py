import numpy

class RegularGridCartesian2D:
    
    def __init__(self,**kwargs):
        """Intitalise a grid"""
        
        if kwargs.has_key('x0') and kwargs.has_key('y0') and \
          kwargs.has_key('nx') and kwargs.has_key('ny') and \
          kwargs.has_key('dx') and kwargs.has_key('dy'):
            
            self.x0 = kwargs['x0']   
            self.y0 = kwargs['y0']
            
            self.nx = kwargs['nx']
            self.ny = kwargs['ny']
            
            self.dx = kwargs['dx']
            self.dy = kwargs['dy']
            
        if kwargs.has_key('values'):
            self.values=kwargs['values']
        else:            
            self.values=numpy.zeros([self.nx,self.ny])
    
    def __div__(self,scalar):
        """ Divide a grid by a scalar
        
        Overrides the default div operator an allows to divide a grid by a scalar using
        new_grid = old_grid / scalar
        
        """
        values=self.values/scalar
        new_obj=RegularGridCartesian2D(x0=self.x0,y0=self.y0,nx=self.nx,ny=self.ny,dx=self.dx,dy=self.dy,values=values)
        return new_obj
        
    def set_value(self,i,j,val):
        """Set a the value of the grid at (i,j)"""
        self.values[i,j]=val
    
    def get_dx_dy(self):
        """Return the grid spacing."""
        return self.dx,self.dy
        
    def get_value(self,i,j):
        """Get value at index i,j."""
        return self.values[i,j]
    
    def set_grid_to_value(self,val):
        self.values=numpy.ones([self.nx,self.ny])*val

    def get_xyz(self):
        """Return all (x,y,z) triplets as an array."""
        values=[]
        for i in range(0,self.nx):
            for j in range(0,self.ny):
                x=self.x0+i*self.dx
                y=self.y0+j*self.dy        
                values.append([x,y,self.get_value(i,j)])
        return numpy.array(values)
        
    def get_linear_interpolated_value(self,x,y):
        """Linear interpolation of a value at (x,y)"""
        p=int((x-self.x0)/self.dx)-1
        q=int((y-self.y0)/self.dy)-1

        s=(x-(p*self.dx+self.x0))/self.dx
        t=(y-(q*self.dy+self.y0))/self.dy

        bs=numpy.zeros([2])
        bs[0]=1.0-s
        bs[1]=s

        bt=numpy.zeros([2])
        bt[0]=1.0-t
        bt[1]=t

        intp=0.0

        for i in range(0,2):
            for j in range(0,2):
                m=p+i
                n=q+j
                if (m>=self.nx):
                    m=self.nx-1

                if (n>=self.ny):
                    n=self.ny-1

                if (m<0):
                    m=0

                if (n<0):
                    n=0

                intp=intp+bs[i]*bt[j]*self.values[m,n]

        return intp
    
